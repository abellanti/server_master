#include <ESP8266WiFi.h>
#include "ESPAsyncWebServer.h"

const int redButton=16;
const int yellowButton=5;
const int greenButton=4;
const char* ssid = "5G-Bill-Gates-Mental-Control";
const char* password = "123456789";
AsyncWebServer server(80);
String color="";


String getColor(){
    return color;
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(redButton, INPUT_PULLUP);
  pinMode(yellowButton, INPUT_PULLUP);
  pinMode(greenButton, INPUT_PULLUP);
  WiFi.softAP(ssid, password);
  IPAddress IP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(IP);
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", getColor().c_str());
  });
  server.begin();
}

void loop() {
  // put your main code here, to run repeatedly:
  if(digitalRead(redButton)){
   color = "red";
   delay(250);
  }
  else if(digitalRead(yellowButton)){
    color = "yellow";
    delay(250);  
  }
  else if(digitalRead(greenButton)){
    color = "green";
    delay(250);
  }
}
